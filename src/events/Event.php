<?php
namespace ChargeWorker\events;
use Exception;
class Event
{
    /**
     * 注释:查询充电站端口总数
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port_num' 设备端口数量
     *  -'start_item' 第一个端口的名称
     * @param $device
     */
    public static function onQueryPortTotal(array $data,$device){
        //处理业务
    }
    /**
     * 注释:查询所有空闲(可用)的充电站端口
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port_num' 设备空闲端口数量
     *  -'list' 空闲的端口 [1,2,4,7] 1、2、4、7号端口空闲
     * @param $device
     */
    public static function onQueryFreePortTotal(array $data,$device){
        //处理业务
    }
    /**
     * 注释:查询消费总额数据
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'card_money' 机器消费刷卡总金额（元）
     *  -'coin_money' 机器消费刷卡总金额（元）
     * @param $device
     */
    public static function onQueryMoneyTotal(array $data,$device){
        //处理业务
    }
    /**
     * 注释:设置 IC 卡、投币器是否可用
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'state' 默认0
     * @param $device
     */
    public static function onIcCardAndCoinSet(array $data,$device){
        //处理业务
    }
    /**
     * 注释:设备上报异常
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port' 充电端口号，0x02 表示2 号端口。若错误的地方不包括端口号，则填写0xFF。
     *  -'error_code' 错误码
     * @param $device
     */
    public static function onDeviceFault(array $data,$device){
        //处理业务
    }
    /**
     * 注释:获取系统时间
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'state' 默认0
     * @param $device
     */
    public static function onFetchTime(array $data,$device){
        //处理业务
    }
    /**
     * 注释:解锁或者锁定某个端口
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port' 端口号
     * @param $device
     */
    public static function onPortLock(array $data,$device){
        //处理业务
    }
    /**
     * 注释:远程停止某个端口的充电
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port' 端口号
     *  -'time' 充电时间
     * @param $device
     */
    public static function onRemoteStop(array $data,$device){
        //处理业务
    }
    /**
     * 注释:读取设备每个端口的状态
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'port_num' 端口数量
     *  -'list' 端口状态
     * @param $device
     */
    public static function onReadPortSate(array $data,$device){
        //处理业务
    }
    /**
     * 注释:设置系统参数
     * 创建者:JSL
     * 时间:2023/06/25 025 下午 01:58
     * @param array $data
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -'state' 默认0
     * @param $device
     */
    public static function onsetConfig(array $data,$device){
        //处理业务
    }
    /**
     * 注释:
     * 创建者:JSL
     * 时间:2023/06/25 025 上午 11:11
     * @param array $data 数据
     * -'imei' 设备编号
     * -'dec' 十进制
     * -'hex' 十六进制
     * -'param'
     *  -''signal 信号值
     * @param $device
     */
    public static function onSignal(array $data,$device){
        //处理业务
    }



    public static function onPortStatus($data){
        //处理业务
        var_dump(implode(' ',$data['hex']));

    }

    /**
     * 注释:异常回调
     * 创建者:JSL
     * 时间:2023/06/25 025 上午 11:14
     * @param Exception $e 异常类
     */
    public static function onError(Exception $e){
        //处理业务

    }
}